from setuptools import setup

setup(name='joke',
      version='0.0.2',
      description='The funniest joke in the world',
      url='https://gitlab.com/VinceDuve/joke.git',
      author='Vince Duve',
      author_email='flyingcircus@example.com',
      license='MIT',
      packages=['joke'],
      zip_safe=False)